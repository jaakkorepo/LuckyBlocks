# LuckyBlocks

Spigot plugin for lucky blocks

## Installing

* Clone the repo
* Open in Eclipse (or your IDE of choice) and export into a jar
* Install Spigot and place in the plugins folder

## Authors

See the list of [contributors](https://github.com/jaqreven/LuckyBlocks/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
