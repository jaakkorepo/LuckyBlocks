package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.jaakkorepo.luckyblocks.Functions.Random;

public class Materials {
	
	public static void villagerStuff(Location l, Player p) {
		Flexible.spawnItem(Material.LEATHER, Random.randomNumber(10, 30), l, p, "");
		Flexible.spawnItem(Material.EMERALD, Random.randomNumber(30, 50), l, p, "");
		Flexible.spawnItem(Material.ROTTEN_FLESH, Random.randomNumber(90, 180), l, p, "");
		Flexible.spawnItem(Material.STRING, Random.randomNumber(30, 60), l, p, "");
		Flexible.spawnItem(Material.PAPER, Random.randomNumber(40, 90), l, p, "");
		Flexible.spawnItem(Material.COAL, Random.randomNumber(30, 50), l, p, "");
	}
	
	public static void ores(Location l, Player p) {
		// TODO: add fireworks, items should be emeralds, diamonds, gold, iron

		World world = p.getWorld();

		int diamondDropRate = 10;
		int diamondRand = 0 + (int) (Math.random() * diamondDropRate);

		for (int i = 0; i < diamondRand; ++i) {
			world.dropItemNaturally(l, new ItemStack(Material.DIAMOND));
		}

		int goldDropRate = 20;
		int goldRand = 5 + (int) (Math.random() * goldDropRate);

		for (int i = 0; i < goldRand; ++i) {
			world.dropItemNaturally(l, new ItemStack(Material.GOLD_INGOT));
		}

		int redstoneDropRate = 45;
		int redstoneRand = 20 + (int) (Math.random() * redstoneDropRate);

		for (int i = 0; i < redstoneRand; ++i) {
			world.dropItemNaturally(l, new ItemStack(Material.REDSTONE));
		}

		int lapisDropRate = 5;
		int lapisRand = 1 + (int) (Math.random() * lapisDropRate);

		for (int i = 0; i < lapisRand; ++i) {
			world.dropItemNaturally(l, new ItemStack(Material.LAPIS_BLOCK));
		}

		int coalDropRate = 32;
		int coalRand = 32 + (int) (Math.random() * coalDropRate);

		for (int i = 0; i < coalRand; ++i) {
			world.dropItemNaturally(l, new ItemStack(Material.COAL));
		}
	}
}
