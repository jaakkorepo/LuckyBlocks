package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Player;

public class Entities {
	public static void elderGuardian(Location l, Player p) {
		World w = p.getWorld();
		for (int i = 0; i < 2; ++i) {
			Guardian guardian = (Guardian) w.spawnEntity(l, EntityType.GUARDIAN);
			guardian.setElder(true);
		}
	}
}
