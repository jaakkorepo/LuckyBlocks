package com.jaakkorepo.luckyblocks.Items;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.jaakkorepo.luckyblocks.Functions.ConfigValues;

public class Armor {
	public static void luckyHelmet(Location l, Player p) {

		World world = p.getWorld();

		ItemStack helmet = (new ItemStack(Material.DIAMOND_HELMET));
		{
			ItemMeta meta = helmet.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 2, true);
			meta.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
			meta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 1, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckyhelmet"));

			helmet.setItemMeta(meta);
		}
		world.dropItemNaturally(l, helmet);
	}

	public static void luckyChestplate(Location l, Player p) {

		World world = p.getWorld();

		ItemStack chestPlate = (new ItemStack(Material.DIAMOND_CHESTPLATE));
		{
			ItemMeta meta = chestPlate.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 2, true);
			meta.addEnchant(Enchantment.PROTECTION_FIRE, 1, true);
			meta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 2, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckychestplate"));

			chestPlate.setItemMeta(meta);
		}
		world.dropItemNaturally(l, chestPlate);
	}
	
	public static void luckyPants(Location l, Player p) {

		World world = p.getWorld();

		ItemStack pants = (new ItemStack(Material.DIAMOND_LEGGINGS));
		{
			ItemMeta meta = pants.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 2, true);
			meta.addEnchant(Enchantment.PROTECTION_FIRE, 2, true);
			meta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 2, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckypants"));

			pants.setItemMeta(meta);
		}
		world.dropItemNaturally(l, pants);
	}
	
	public static void luckyBoots(Location l, Player p) {

		World world = p.getWorld();

		ItemStack boots = (new ItemStack(Material.DIAMOND_BOOTS));
		{
			ItemMeta meta = boots.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
			meta.addEnchant(Enchantment.PROTECTION_EXPLOSIONS, 2, true);
			meta.addEnchant(Enchantment.PROTECTION_FALL, 4, true);
			meta.addEnchant(Enchantment.PROTECTION_PROJECTILE, 2, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckyboots"));

			boots.setItemMeta(meta);
		}
		world.dropItemNaturally(l, boots);
	}
	
	public static void goldArmor(Location l, Player p) {
		World w = p.getWorld();
		
		w.dropItemNaturally(l, new ItemStack(Material.GOLD_BOOTS));
		w.dropItemNaturally(l, new ItemStack(Material.GOLD_LEGGINGS));
		w.dropItemNaturally(l, new ItemStack(Material.GOLD_CHESTPLATE));
		w.dropItemNaturally(l, new ItemStack(Material.GOLD_HELMET));
	}

	public static void chainArmor(Location l, Player p) {
		World w = p.getWorld();
		
		List<ItemStack> list = new ArrayList<ItemStack>();
		
		list.add(new ItemStack(Material.CHAINMAIL_BOOTS));
		list.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
		list.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		list.add(new ItemStack(Material.CHAINMAIL_HELMET));
		
		Collections.shuffle(list);
		
		int rand = 2 + (int)(Math.random() * ((3 - 2) + 1));
		
		for (int i = 1; i < (1 + rand); ++i) {
			w.dropItemNaturally(l, list.get(i));
		}
	}

	public static void ironArmor(Location l, Player p) {
		World w = p.getWorld();
		
		List<ItemStack> list = new ArrayList<ItemStack>();
		
		list.add(new ItemStack(Material.IRON_BOOTS));
		list.add(new ItemStack(Material.IRON_LEGGINGS));
		list.add(new ItemStack(Material.IRON_CHESTPLATE));
		list.add(new ItemStack(Material.IRON_HELMET));
		
		Collections.shuffle(list);
		
		int rand = 2 + (int)(Math.random() * ((3 - 2) + 1));
		
		for (int i = 1; i < (1 + rand); ++i) {
			w.dropItemNaturally(l, list.get(i));
		}
	}

	public static void diamondArmor(Location l, Player p) {
		World w = p.getWorld();
		
		List<ItemStack> list = new ArrayList<ItemStack>();
		
		list.add(new ItemStack(Material.DIAMOND_BOOTS));
		list.add(new ItemStack(Material.DIAMOND_LEGGINGS));
		list.add(new ItemStack(Material.DIAMOND_CHESTPLATE));
		list.add(new ItemStack(Material.DIAMOND_HELMET));
		
		Collections.shuffle(list);
		
		int rand = 1 + (int)(Math.random() * ((2 - 1) + 1));
		
		for (int i = 1; i < (1 + rand); ++i) {
			w.dropItemNaturally(l, list.get(i));
		}
	}
}
