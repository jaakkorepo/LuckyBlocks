package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Food {
	public static void vegetables(Location l, Player p) {
		int rand = 8 + (int) (Math.random() * ((16 - 8) + 1));
		for (int i = 8; i < (1 + rand); ++i) {
			World w = p.getWorld();

			int howManyPossibleHappenstances = 6;

			int randomChoice = 1 + (int) (Math.random() * howManyPossibleHappenstances);

			switch (randomChoice) {
			case 1:
				w.dropItemNaturally(l, new ItemStack(Material.POISONOUS_POTATO));
				break;
			case 2:
				w.dropItemNaturally(l, new ItemStack(Material.POTATO));
				break;
			case 3:
			case 4:
				w.dropItemNaturally(l, new ItemStack(Material.BAKED_POTATO));
				break;
			case 5:
			case 6:
				w.dropItemNaturally(l, new ItemStack(Material.CARROT));
				break;
			}
		}
	}

	public static void meats(Location l, Player p) {
		World w = p.getWorld();

		int rand = 8 + (int) (Math.random() * ((16 - 8) + 1));
		for (int i = 8; i < (1 + rand); ++i) {

			int howManyPossibleHappenstances = 16;

			int randomChoice = 1 + (int) (Math.random() * howManyPossibleHappenstances);

			switch (randomChoice) {
			case 1:
			case 2:
				w.dropItemNaturally(l, new ItemStack(Material.COOKED_CHICKEN));
				break;
			case 3:
			case 4:
				w.dropItemNaturally(l, new ItemStack(Material.RAW_CHICKEN));
				break;
			case 5:
			case 6:
				w.dropItemNaturally(l, new ItemStack(Material.COOKED_FISH));
				break;
			case 7:
			case 8:
				w.dropItemNaturally(l, new ItemStack(Material.RAW_FISH));
				break;
			case 9:
			case 10:
				w.dropItemNaturally(l, new ItemStack(Material.COOKED_BEEF));
				break;
			case 11:
			case 12:
				w.dropItemNaturally(l, new ItemStack(Material.RAW_BEEF));
				break;
			case 13:
			case 14:
				w.dropItemNaturally(l, new ItemStack(Material.PORK));
				break;
			case 15:
			case 16:
				w.dropItemNaturally(l, new ItemStack(Material.GRILLED_PORK));
				break;
			}
		}
	}
}
