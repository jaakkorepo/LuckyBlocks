package com.jaakkorepo.luckyblocks.Items;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.md_5.bungee.api.ChatColor;

public class Flexible {
	public static void spawnItem(Material material, Integer amount, Location l, Player p, String string) {
		World w = p.getWorld();
		ItemStack item = new ItemStack(material, amount);
		if (StringUtils.isNotBlank(string.toString())) {
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(ChatColor.WHITE + string.toString());
			item.setItemMeta(im);
		}
		w.dropItemNaturally(l, item);
	}

	public static void addPotionEffect(PotionEffectType effect, int duration, int amplifier, Player p) {
		p.addPotionEffect((new PotionEffect(effect, duration, amplifier)));
	}
	
	public static void spawnEntities(EntityType entity, int amount, String name, Location l, Player p) {
		World w = p.getWorld();
		for (int i = 0; i < amount; ++i) {
			w.spawnEntity(l, entity).setCustomName(name);
		}
	}
}
