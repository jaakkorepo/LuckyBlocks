package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.jaakkorepo.luckyblocks.Functions.ConfigValues;
import com.jaakkorepo.luckyblocks.Functions.Random;

public class Weapons {
	public static void luckyBow(Location l, Player p) {

		World world = p.getWorld();

		ItemStack bow = (new ItemStack(Material.BOW));
		{
			ItemMeta meta = bow.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.ARROW_DAMAGE, 5, true);
			meta.addEnchant(Enchantment.ARROW_KNOCKBACK, 2, true);
			meta.addEnchant(Enchantment.ARROW_FIRE, 1, false);
			meta.addEnchant(Enchantment.ARROW_INFINITE, 1, false);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckybow"));

			bow.setItemMeta(meta);
		}
		world.dropItemNaturally(l, bow);
	}
	
	public static void bowAndArrows(Location l, Player p) {
		Flexible.spawnItem(Material.BOW, 1, l, p, "");
		Flexible.spawnItem(Material.ARROW, Random.randomNumber(24, 48), l, p, "");
	}

	public static void luckySword(Location l, Player p) {

		World world = p.getWorld();

		ItemStack sword = (new ItemStack(Material.DIAMOND_SWORD));
		{
			ItemMeta meta = sword.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
			meta.addEnchant(Enchantment.KNOCKBACK, 1, true);
			meta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
			meta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 1, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckysword"));

			sword.setItemMeta(meta);
		}
		world.dropItemNaturally(l, sword);
	}
	
	public static void zombieHead(Location l, Player p) {
		World world = p.getWorld();
		ItemStack head = (new ItemStack(Material.SKULL_ITEM, 1, (short) 2)); // Zombie head
		{
			ItemMeta meta = head.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.DAMAGE_ALL, 6, true);
			meta.addEnchant(Enchantment.KNOCKBACK, 3, true);
			meta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 3, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.zombiehead"));

			head.setItemMeta(meta);
		}
		world.dropItemNaturally(l, head);
	}
	
	public static void fireSword(Location l, Player p) {
		
		World world = p.getWorld();
		ItemStack sword = (new ItemStack(Material.DIAMOND_SWORD));
		{
			ItemMeta meta = sword.getItemMeta();
			meta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
			meta.setDisplayName(ConfigValues.getLangValue("items.firesword"));
			
			sword.setItemMeta(meta);
		}
		world.dropItemNaturally(l,  sword);
		
	}
	
	public static void baseballBat(Location l, Player p) {
		
		World world = p.getWorld();
		ItemStack bat = (new ItemStack(Material.WOOD_SPADE));
		{
			ItemMeta meta = bat.getItemMeta();
			meta.addEnchant(Enchantment.KNOCKBACK, 7, true);
			meta.setDisplayName(ConfigValues.getLangValue("items.baseballbat"));
			
			bat.setItemMeta(meta);
		}
		world.dropItemNaturally(l,  bat);
		
	}
	
	public static void sharpHoe(Location l, Player p) {
		
		World world = p.getWorld();
		ItemStack hoe = (new ItemStack(Material.DIAMOND_HOE));
		{
			ItemMeta meta = hoe.getItemMeta();
			meta.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
			meta.setDisplayName(ConfigValues.getLangValue("items.sharphoe"));
			
			hoe.setItemMeta(meta);
		}
		world.dropItemNaturally(l,  hoe);
		
	}

	public static void herobrineStick(Location l, Player p) {

		World world = p.getWorld();

		ItemStack stick = (new ItemStack(Material.STICK));
		{
			ItemMeta meta = stick.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 10, true);
			meta.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
			meta.addEnchant(Enchantment.FIRE_ASPECT, 3, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.herobrinestick"));

			stick.setItemMeta(meta);
		}
		world.dropItemNaturally(l, stick);
	}

	public static void woodSword(Location l, Player p) {

		World world = p.getWorld();

		ItemStack woodSword = (new ItemStack(Material.WOOD_SWORD));
		{
			ItemMeta meta = woodSword.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 10, true);
			meta.addEnchant(Enchantment.DAMAGE_ALL, 3, true);
			meta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.woodsword"));

			woodSword.setItemMeta(meta);
		}
		world.dropItemNaturally(l, woodSword);
	}

	public static void fightingSpade(Location l, Player p) {

		World world = p.getWorld();

		ItemStack fightingSpade = (new ItemStack(Material.DIAMOND_SPADE));
		{
			ItemMeta meta = fightingSpade.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 10, true);
			meta.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
			meta.addEnchant(Enchantment.KNOCKBACK, 2, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.warspade"));

			fightingSpade.setItemMeta(meta);
		}
		world.dropItemNaturally(l, fightingSpade);
	}
}
