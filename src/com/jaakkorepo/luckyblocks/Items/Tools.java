package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.jaakkorepo.luckyblocks.Functions.ConfigValues;

public class Tools {
	public static void luckyRod(Location l, Player p) {

		World world = p.getWorld();

		ItemStack fishingRod = (new ItemStack(Material.FISHING_ROD));
		{
			ItemMeta meta = fishingRod.getItemMeta();
			meta.addEnchant(Enchantment.DURABILITY, 3, true);
			meta.addEnchant(Enchantment.LUCK, 3, true);
			meta.addEnchant(Enchantment.DIG_SPEED, 3, true);

			meta.setDisplayName(ConfigValues.getLangValue("items.luckyrod"));

			fishingRod.setItemMeta(meta);
		}
		world.dropItemNaturally(l, fishingRod);
	}
}
