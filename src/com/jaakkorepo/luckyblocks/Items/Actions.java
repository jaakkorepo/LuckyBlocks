package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Actions {
	public static void ignitePlayer(int duration, Player p) {
		p.setFireTicks(duration);
	}

	public static void strikePlayer(int count, Player p) {
		World w = p.getWorld();
		for (int i = 0; i < count; ++i) {
			w.strikeLightning(p.getLocation());
		}
	}

	public static void launchPlayer(int x, int y, int z, Player p) {
		p.setVelocity(new Vector(x, y, z));
	}
}
