package com.jaakkorepo.luckyblocks.Items;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class FallingBlock {
	@SuppressWarnings("deprecation")
	public static void Rainbow(Location l, Player p) {
		for (int i = 1; i < 8; ++i) {
			if (i == 7) {
				l.getWorld().spawnFallingBlock(l.add(0, 5 + (i * 1.5), 0), Material.DIAMOND_BLOCK, (byte) 0);
			} else {
				l.getWorld().spawnFallingBlock(l.add(0, 5 + (i * 1.5), 0), Material.STAINED_CLAY, (byte) i);
			}
		}
	}
}
