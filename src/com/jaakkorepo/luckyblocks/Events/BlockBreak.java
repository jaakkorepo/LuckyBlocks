package com.jaakkorepo.luckyblocks.Events;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.jaakkorepo.luckyblocks.Functions.LuckyBlock;

public class BlockBreak implements Listener {

	@EventHandler
	public void onBreak(BlockBreakEvent event) {

		Block block = event.getBlock();
		if (block.getType().equals(Material.SPONGE)) { // it's a sponge!

			LuckyBlock.getRandom(event);
			

		}
	}

}
