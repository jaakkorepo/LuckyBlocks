package com.jaakkorepo.luckyblocks.Functions;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.potion.PotionEffectType;

import com.jaakkorepo.luckyblocks.Items.Actions;
import com.jaakkorepo.luckyblocks.Items.Armor;
import com.jaakkorepo.luckyblocks.Items.Entities;
import com.jaakkorepo.luckyblocks.Items.FallingBlock;
import com.jaakkorepo.luckyblocks.Items.Flexible;
import com.jaakkorepo.luckyblocks.Items.Materials;
import com.jaakkorepo.luckyblocks.Items.Weapons;

public class LuckyBlock {

	@SuppressWarnings("deprecation")
	public static void title(String eventResult, Player p) {
		p.sendTitle(ConfigValues.getLangValue("title.title"),
				String.format(ConfigValues.getLangValue("title.subtitle"), eventResult));
	}

	public static int events() {
		return 50; // switch events count
	}

	public static void getRandom(BlockBreakEvent event) {
		Location l = event.getBlock().getState().getLocation();
		Player p = event.getPlayer();

		p.getServer().getConsoleSender()
				.sendMessage(String.format(ConfigValues.getLangValue("logs.open"), p.getName()));

		event.setCancelled(true); // No drops: doesnt break the block -> deletes it
		event.getPlayer().getWorld().getBlockAt(event.getBlock().getLocation()).setType(Material.AIR);

		int randomChoice = 1 + (int) (Math.random() * events());
		launchBlock(l, p, randomChoice);
	}

	public static void launchBlock(Location l, Player p, int choice) {

		Flexible.spawnItem(Material.COOKED_BEEF, Random.randomNumber(3, 5), l, p, ""); // 3-5 cooked beef
		Flexible.spawnItem(Material.WOOD, Random.randomNumber(8, 16), l, p, ""); // 8-16 planks

		switch (choice) {
		case 1:
			Armor.luckyHelmet(l, p);
			break; // Lucky helmet!
		case 2:
			Armor.luckyChestplate(l, p);
			break; // Lucky chestplate!
		case 3:
			Weapons.luckySword(l, p);
			break; // Lucky sword!
		case 4:
			Flexible.spawnItem(Material.LAVA_BUCKET, Random.randomNumber(2, 4), l, p, "");
			break; // 2-4 lava buckets
		case 5:
			Flexible.spawnItem(Material.DIAMOND, Random.randomNumber(8, 16), l, p, "");
			break; // diamonds
		case 6:
			Flexible.spawnItem(Material.FLINT_AND_STEEL, 1, l, p, "");
			break; // flint and steel
		case 7:
			Flexible.spawnEntities(EntityType.SKELETON, Random.randomNumber(3, 5), "", l, p);
			break; // 3-5 Skeletons
		case 8:
			Weapons.luckyBow(l, p);
			break; // Lucky bow
		case 9:
			Flexible.addPotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000, 0, p);
			break; // strength 1 for 50s
		case 10:
			Actions.launchPlayer(0, 48, 0, p);
			break; // launches player in the air
		case 11:
			Actions.ignitePlayer(300, p);
			break; // ignite player for 15s
		case 12:
			Actions.strikePlayer(5, p);
			break;
		case 13:
			Weapons.herobrineStick(l, p);
			break; // Herobrines stick
		case 14:
			Weapons.woodSword(l, p);
			break; // Wooden sword with enchantments
		case 15:
			Flexible.spawnEntities(EntityType.SILVERFISH, Random.randomNumber(2, 4), "", l, p);
			break;
		case 16:
			Flexible.spawnEntities(EntityType.BLAZE, 1, "Roihu!", l, p);
			break;
		case 17:
			Flexible.addPotionEffect(PotionEffectType.SPEED, 60 * 20, 2, p);
			break; // speed 3 for 60s
		case 18:
			Flexible.addPotionEffect(PotionEffectType.CONFUSION, 10 * 20, 0, p);
			break; // nausea for 10s
		case 19:
			Armor.diamondArmor(l, p);
			break; // few diamond armor pieces
		case 20:
			Weapons.fightingSpade(l, p);
			break; // Fighting spade
		case 21:
			Flexible.spawnEntities(EntityType.WITHER, 1, "", l, p);
			break;
		case 22:
			Flexible.addPotionEffect(PotionEffectType.JUMP, 120 * 20, 9, p);
			break; // jump boost 10 120s
		case 23:
			Flexible.spawnItem(Material.GOLD_INGOT, Random.randomNumber(32, 96), l, p, "");
			break; // gold
		case 24:
			Flexible.spawnEntities(EntityType.CAVE_SPIDER, Random.randomNumber(2, 4),
					ConfigValues.getLangValue("mobs.sven"), l, p);
			break; // Cave spiders
		case 25:
			Flexible.spawnItem(Material.DIAMOND_BLOCK, 1, l, p, ConfigValues.getLangValue("items.diamondblock"));
			break;
		case 26:
			Weapons.fireSword(l, p); // fire aspect 1 dia sword
			break;
		case 27:
			Weapons.sharpHoe(l, p); // sharp 5 dia hoe
			break;
		case 28:
			Flexible.spawnItem(Material.WOOD, Random.randomNumber(64, 256), l, p, "");
			break;
		case 29:
			Flexible.spawnEntities(EntityType.GUARDIAN, Random.randomNumber(6, 12),
					ConfigValues.getLangValue("mobs.guardianarmy"), l, p);
			break;
		case 30:
			Flexible.spawnEntities(EntityType.BLAZE, Random.randomNumber(5, 8), "", l, p);
			break;
		case 31:
			Flexible.spawnEntities(EntityType.VILLAGER, Random.randomNumber(7, 13), "", l, p);
			Materials.villagerStuff(l, p);
			break;
		case 32:
			Weapons.zombieHead(l, p);
			break;
		case 33:
			LoadSchematic.loadSchematic(p, "temple1");
			break;
		case 34:
			LoadSchematic.loadSchematic(p, "temple2");
			break;
		case 35:
			Entities.elderGuardian(l, p);
			break;
		case 36:
			Armor.luckyPants(l, p);
			break;
		case 37:
			Armor.luckyBoots(l, p);
			break;
		case 38:
			Flexible.spawnEntities(EntityType.ENDERMITE, Random.randomNumber(2, 5), "", l, p);
			break;
		case 39:
			Weapons.baseballBat(l, p);
			break;
		case 40:
			Armor.chainArmor(l, p);
			break;
		case 41:
			Armor.goldArmor(l, p);
			break;
		case 42:
			Armor.ironArmor(l, p);
			break;
		case 43:
			FallingBlock.Rainbow(l, p);
			break;
		case 44:
			LoadSchematic.loadSchematic(p, "luckywell");
			break;
		case 45:
			LoadSchematic.loadSchematic(p, "hugeblock");
			break;
		case 46:
			Flexible.spawnItem(Material.ENDER_PEARL, Random.randomNumber(2, 4), l, p, "");
			break;
		case 47:
			Flexible.spawnItem(Material.SNOW_BALL, Random.randomNumber(32, 96), l, p, "");
			break;
		case 48:
			Flexible.spawnItem(Material.EGG, Random.randomNumber(32, 96), l, p, "");
			break;
		case 49:
			Flexible.spawnItem(Material.GOLDEN_APPLE, Random.randomNumber(6, 12), l, p, "");
			break;
		case 50:
			Weapons.bowAndArrows(l, p);
			break;
		}

		title(ConfigValues.getLangValue("loot." + String.valueOf(choice)), p);
		// debug items here
	}
}
