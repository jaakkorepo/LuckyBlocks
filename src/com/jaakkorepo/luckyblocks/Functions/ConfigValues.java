package com.jaakkorepo.luckyblocks.Functions;

import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import com.jaakkorepo.luckyblocks.LuckyBlocks;

public class ConfigValues {
	private static Plugin plugin = LuckyBlocks.getPlugin(LuckyBlocks.class);

	public static String getLangValue(String value) {
		try {
			String string = plugin.getConfig().get("translations." + value + "." + getConfigValue("lang")).toString();
			return ChatColor.translateAlternateColorCodes('&', string);
		} catch (Exception e) {
			return "undefined";
		}
		
	}

	public static String getConfigValue(String value) {
		try {
			return plugin.getConfig().get(value).toString();
		} catch (Exception e) {
			return null;
		} 
	}
}
