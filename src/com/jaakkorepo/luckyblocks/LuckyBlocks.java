package com.jaakkorepo.luckyblocks;
import org.bukkit.plugin.java.JavaPlugin;

import com.jaakkorepo.luckyblocks.Commands.CommandLB;
import com.jaakkorepo.luckyblocks.Events.BlockBreak;
import com.jaakkorepo.luckyblocks.Functions.ConfigValues;

public class LuckyBlocks extends JavaPlugin {
    // Fired when plugin is first enabled
    @Override
    public void onEnable() {
    	saveDefaultConfig();
    	getServer().getConsoleSender().sendMessage(
    			ConfigValues.getLangValue("logs.enable")	
    	);
    	getServer().getPluginManager().registerEvents(new BlockBreak(), this);
    	this.getCommand("lb").setExecutor(new CommandLB());
    }
    // Fired when plugin is disabled
    @Override
    public void onDisable() {
    	getServer().getConsoleSender().sendMessage(
    			ConfigValues.getLangValue("logs.disable")	
    	);
    }
    
	public void loadConfig(){
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
    
    
}
