package com.jaakkorepo.luckyblocks.Commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.jaakkorepo.luckyblocks.Functions.LuckyBlock;
import com.jaakkorepo.luckyblocks.Functions.ConfigValues;

public class CommandLB implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (args.length > 0) {
				if (StringUtils.isNumeric(args[0])) { // is a number

					int number = Integer.parseInt(args[0]);
					if (number >= 1 && number <= LuckyBlock.events()) { // Valid option
						LuckyBlock.launchBlock(player.getLocation(), player, Integer.parseInt(args[0]));
					} else {
						player.sendMessage(ConfigValues.getLangValue("commands.prefix") + String
								.format(ConfigValues.getLangValue("commands.invalidnum"), 1, LuckyBlock.events()));
					}
				} else {
					player.sendMessage(ConfigValues.getLangValue("commands.prefix")
							+ ConfigValues.getLangValue("commands.unknownarg"));
				}

			} else {

				player.sendMessage(String.format(ConfigValues.getLangValue("commands.lbhome"),
						ConfigValues.getLangValue("commands.divider")));

			}
		}

		// If the player (or console) uses our command correct, we can return true
		return true;
	}
}